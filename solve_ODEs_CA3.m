%%
% Computational Assignment 3 : Solving a system of ODEs with built-in
% solvers
%
% SAADBIN KHAN
%
% Following are the system of four differential equations that we will
% solve:
%
% $\frac{dF_A}{dV}=r_A$
%
% $\frac{dF_B}{dV}=r_B$
%
% $\frac{dF_C} {dV} = r_C$
%
% $\frac{dT}{dV} = \frac{U_a(T_a-T)+(-r_{1A})(-H_{Rx1A})+(-r_{2A})(-H_{Rx2A})}{F_AC_{P_A} + F_BC_{P_B} + F_CC_{P_C}}$
%
% Inside the function ODE_CA3 we define all the relations necessary for the
% buildup of the ODEs that we will solve. The relations are as follows:
% 
% $k_1A = 10 exp[\frac {E_1}{R} (\frac {1}{300}-\frac {1}{T})]s^{-1}$
%
% $k_2A = 0.09 exp[\frac {E_2}{R} (\frac {1} {300}-\frac {1}{T})] \frac{dm^3}{mol.s}$
%
% $\frac{E_1}{R} = 4000K; \frac{E_2}{R} = 9000K;$  
% 
% $C_A = C_{T_0}(\frac{F_A}{F_T})(\frac{T_0}{T})$
%
% $C_B = C_{T_0}(\frac{F_B}{F_T})(\frac{T_0}{T})$
%
% $C_C = C_{T_0}(\frac{F_C}{F_T})(\frac{T_0}{T})$
%
% $r_1A = -k_{1A} C_A
%
% $r_2A = -k_{2A} C_A^2$
%
% $r_A = r_{1A}+r_{2A}$
% $r_B = -r_{1A}$
% $r_C = \frac{-r_{2A}}{2}$
%
% inputs are:  
% $U_a, T_a, C_{P_A}, C_{P_B}, C_{P_C}, \Delta H_{r_1A}, \Delta H_{r_1A}, \frac{E_1}{R}, \frac{E_2}{R}$
%
% outputs are: 
% $F_A, F_B, F_C, T$

%%

function solve_ODEs_CA3

%%%%%%%%%%% Clearing prior data %%%%%%%%%%%
clc
clear all

%%%%%%%%%%% Parameters required %%%%%%%%%%%
Ua = 4000; % J/m3.s.C
Ta = 373; % Kelvin
CPA = 90; % J/mol.C
CPB = 90; % J/mol.C
CPC = 180; % J/mol.C
delHR1A = -20000; % J/(mol of A reacted in reaction 1)
delHR2A = -60000; % J/(mol of A reacted in reaction 2)
E1overR = 4000; % Kelvin
E2overR = 9000; % Kelvin
CT0 = 0.1; % mol/dm3
T0 = 423; % Kelvin

%%%%%%%%%%% Solver paramters %%%%%%%%%%%
init = [100 0 0 423]; % Initial values 
vspan = [0.0:0.01:1.0]; % Span of depndent variable V in dm3
[V F_and_T] = ode45(@ODEs_CA3, vspan, init); % Solving the ODE system

%%%%%%%%%%% Plotting %%%%%%%%%%%
figure(1)
plot(V,F_and_T(:,4),'-k','linewidth',2.5) % Figure 1 plot
legend('T')
xlabel('V (dm^3)') 
ylabel('T (K)')

figure(2)
plot(V,F_and_T(:,1),'-g',...
    V,F_and_T(:,2),'--r',...
    V,F_and_T(:,3),':b','linewidth',2.5) % Figure 2 plot
legend('F_A','F_B','F_C')
xlabel('V (dm^3)') 
ylabel('F_i (mol/s)')

%%%%%%%%%%% Function Definition %%%%%%%%%%%
function sysdiff = ODEs_CA3(V, F_and_T); % Function to define the ODE system

FT = F_and_T(1) + F_and_T(2) + F_and_T(3); % mol/s
k1A = 10 * exp( E1overR * (1/300-1/F_and_T(4)) ); % dm3/mol.s
k2A = 0.09 * exp( E2overR * (1/300-1/F_and_T(4)) ); % dm3/mol.s

CA = CT0*(F_and_T(1)/FT)*(T0/F_and_T(4)); % mol/dm3
CB = CT0*(F_and_T(2)/FT)*(T0/F_and_T(4)); % mol/dm3
CC = CT0*(F_and_T(3)/FT)*(T0/F_and_T(4)); % mol/dm3
r1A = -k1A*CA;
r2A = -k2A*CA*CA;
r1B = -r1A;
r2C = -r2A/2;
rA = r1A + r2A;
rB = r1B;
rC = r2C;

dFAdV = rA; % ODE 1
dFBdV = rB; % ODE 2
dFCdV = rC; % ODE 3
dTdV  = ( Ua*(Ta-F_and_T(4)) + (-r1A)*(-delHR1A) + (-r2A)*(-delHR2A) ) /...
    (F_and_T(1)*CPA + F_and_T(2)*CPB + F_and_T(3)*CPC); % ODE 4

sysdiff = [dFAdV; dFBdV; dFCdV; dTdV]; % Output of the ODE system function
end
end
%%%%%%%%%%% Function ends here %%%%%%%%%%%