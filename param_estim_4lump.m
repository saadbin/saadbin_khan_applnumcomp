%%
% Computational Assignment 4
%
% Four Lump Model
%
% SAADBIN KHAN
%
% Following are the system of three differential equations associated with 
% a three lump model involving four subgroups: VGO (y1), gasoline (y2),
% gas (y3) and coke (y4):
%
% $\frac{dy_1}{dt}=-(k_{12}+k_{13}+k_{14}y_{1}^{2}$
%
% $\frac{dy_2}{dt}=k_{12}y_{1}^{2}-k_{23}y_{2}-k_{24}y_{2}$
%
% $\frac{dy_3}{dt}=k_{13}y_{1}^{2}-k_{23}y_{2}$
%
% $\frac{dy_4}{dt}=k_{14}y_{1}^{2}-k_{24}y_{2}$
%
% For this program our inputs are:  
% A set of weight fraction data 'y' with varying time (t)
%
% outputs are: 
% Estimation of parameters 'k'

function param_estim_3lump
clc
clear all

t = [1/60 1/30 1/20 1/10]; % Time (h)
ytable = [.5074 .3796 .2882 .1762; .3767 .4385 .4865 .5416; .0885 .136 .1681 .2108; .0274 .0459 .0572 .0714]; % Available y data 
y(1,:) = ytable(1,:); % VGO
y(2,:) = ytable(2,:); % Gasoline
y(3,:) = ytable(3,:); % Gas
y(4,:) = ytable(4,:); % Coke
convinit= 1-y(1,:); % Initial Conversion

kinit = [0.0 0.0 0.0 0.0 0.0]; % Initial Guess for k paramters

y0 =[1.0 0.0 0.0 0.0]; % Initial y values

[k,resnorm] = lsqcurvefit(@(k,t) ODEsolve(k,t),kinit,t,y); % Fitting Curve

tplot = 0.0:0.01:1.0; % Plotting Timespan
y_calc = ODEsolve(k,tplot); % Calculated y at Plotting Timespan
conv = 1-y_calc(1,:); % Final Conversion

% Predicted Parameters
k_12=k(1) 
k_13=k(2)
k_14=k(3)
k_23=k(4)
k_24=k(5)


% Here, The plotting Starts:

%Figure 1
figure(1)
plot(t,y(1,:),'xk');
hold on
plot(t,y(2,:),'ok');
hold on
plot(t,y(3,:),'vk');
hold on
plot(t,y(4,:),'*k');
hold on
plot(tplot,y_calc(1,:),'b-','linewidth',1.5);
hold on
plot(tplot,y_calc(2,:),'r--','linewidth',1.5);
hold on
plot(tplot,y_calc(3,:),'g:','linewidth',1.5);
hold on
plot(tplot,y_calc(3,:),'c.-','linewidth',1.5);
hold on
xlabel('Time (h)');
ylabel('Yield, wt fraction');
legend('VGO Data','Gasoline Data','Gas Data','Coke Data','Predicted VGO',...
    'Predicted Gasoline','Predicted Gas','Predicted Coke','location','best')
hold off

% Figure 2
figure(2)
plot(convinit,y(1,:),'xk');
hold on
plot(convinit,y(2,:),'ok');
hold on
plot(convinit,y(3,:),'vk');
hold on
plot(convinit,y(4,:),'*k');
hold on
plot([1-y0(1),conv],[y0(1),y_calc(1,:)],'b-','linewidth',1.5);
hold on
plot([1-y0(1),conv],[y0(2),y_calc(2,:)],'r--','linewidth',1.5);
hold on
plot([1-y0(1),conv],[y0(3),y_calc(3,:)],'g:','linewidth',1.5);
hold on
plot([1-y0(1),conv],[y0(4),y_calc(4,:)],'c.-','linewidth',1.5);
xlabel('Conversion, wt fraction');
ylabel('Yield, wt fraction');
legend('VGO Data','Gasoline Data','Gas Data','Coke Data','Predicted VGO',...
    'Predicted Gasoline','Predicted Gas','Predicted Coke','location','best')
hold off

% Function where the system of ODE is defined
function dydt = ODEdef_l1(time,y,k)
    dydt(1) = -(k(1)+k(2)+k(3))*y(1)^2;
    dydt(2) = k(1)*y(1)^2-k(4)*y(2)-k(5)*y(2);
    dydt(3)=k(2)*y(1)^2+k(4)*y(2);
    dydt(4)=k(3)*y(1)^2+k(5)*y(2);
    dydt = dydt';
end

% Function to solve the ODE sytem
function yout = ODEsolve(k,t)
    y0 = [1.0 0.0 0.0 0.0];
    for i=1:length(t)
        tspan = [0:0.01:t(i)+0.01];
        [~,yseries] = ode23s(@(time,y) ODEdef_l1(time,y,k),tspan,y0);
        yout(i,:)=yseries(end,:);
    end
    yout=yout';
end

end 