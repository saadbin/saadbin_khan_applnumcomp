function varargout = GUI_odes(varargin)
% GUI_ODES MATLAB code for GUI_odes.fig
%      GUI_ODES, by itself, creates a new GUI_ODES or raises the existing
%      singleton*.
%
%      H = GUI_ODES returns the handle to a new GUI_ODES or the handle to
%      the existing singleton*.
%
%      GUI_ODES('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI_ODES.M with the given input arguments.
%
%      GUI_ODES('Property','Value',...) creates a new GUI_ODES or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI_odes_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI_odes_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI_odes

% Last Modified by GUIDE v2.5 12-Dec-2018 04:09:27

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI_odes_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI_odes_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before GUI_odes is made visible.
function GUI_odes_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI_odes (see VARARGIN)

% Choose default command line output for GUI_odes
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes GUI_odes wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = GUI_odes_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

diffeqtn=get(handles.rhs,'String')
numer=get(handles.numerator,'String')
denom=get(handles.denominator,'String')
ICvalue=get(handles.initialval,'String')
save('odes.mat','diffeqtn','numer','denom','ICvalue')

% diffeqtn=str2double(diffeqtn)
close('GUI_odes')



function numerator_Callback(hObject, eventdata, handles)
% hObject    handle to numerator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of numerator as text
%        str2double(get(hObject,'String')) returns contents of numerator as a double

save

% --- Executes during object creation, after setting all properties.
function numerator_CreateFcn(hObject, eventdata, handles)
% hObject    handle to numerator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function denominator_Callback(hObject, eventdata, handles)
% hObject    handle to denominator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of denominator as text
%        str2double(get(hObject,'String')) returns contents of denominator as a double


% --- Executes during object creation, after setting all properties.
function denominator_CreateFcn(hObject, eventdata, handles)
% hObject    handle to denominator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rhs_Callback(hObject, eventdata, handles)
% hObject    handle to rhs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% dyexpression=get(handles.rhs,'String');
% dyexpression=str2num(dyexpression);
% save('dyexpression.txt','dyexpression','-ascii')
% numerator=get(handles.numerator,'String');
% denominator=get(handles.denominator,'String');
% init=get(handles.initialval,'String');
% init=str2num(init);
% Hints: get(hObject,'String') returns contents of rhs as text
%        str2double(get(hObject,'String')) returns contents of rhs as a double




% --- Executes during object creation, after setting all properties.
function rhs_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rhs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function initialval_Callback(hObject, eventdata, handles)
% hObject    handle to initialval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of initialval as text
%        str2double(get(hObject,'String')) returns contents of initialval as a double


% --- Executes during object creation, after setting all properties.
function initialval_CreateFcn(hObject, eventdata, handles)
% hObject    handle to initialval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
