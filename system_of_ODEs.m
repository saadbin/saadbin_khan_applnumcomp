%%
% Computational Assignment 2 : Building a System of ODEs
%%
% SAADBIN KHAN
%%
% The following function builds a system of ODEs using variable input arguments. The system is defined by:
%%
% $\frac{dC_A}{dt}=-k_1C_A-k_2C_A$
%%
% $\frac{dC_B}{dt}=k_1C_A-k_3-k_4C_B$
%%
% Here, 
%%
% t is a single time value (second)
%%
% C is a 1 � 2 matrix (row vector) of concentrations (mg/L)
%%
% k1 (1/h), k2 (1/h), k3 (mg/(Lh)), k4 (1/h) are parameter values
%%
% output is a 2 � 1 matrix (column vector) of differential equations evaluated at time t.

% Clearing prior data
%clc
%clear all

% Scripts to test for different cases (printing allowed):
%derivatives = system_of_ODEs(0.0,[6.25 0],0.15,0.6,0.1,0.2) % Case 1 : All Specified
%derivatives = system_of_ODEs(0.0,[6.25 0]) % Case 2 : t & C Specified
%derivatives = system_of_ODEs() % Case 3 : Nothing Specified
%derivatives = system_of_ODEs(1.0) % Error on Exception


%% 
%The function begins here:

function output = system_of_ODEs(varargin)

[r c] = size(varargin); % Reading the size of input argument array
k_def = [{0.15} {0.6} {0.1} {0.2}]; % Input of default k
t_def = 0.0; % Input of default t
C_def = [6.25 0]; % Input of default C

flag_abort = 0; % A flag for error detection

if c == 6 % Case 1
    fprintf('Case 1 : Everything Specified');
    param(1:6) = varargin(1:6);
    
elseif c == 2 % Case 2
    fprintf('Case 2 : Only t & C Specified');
    param(1:2) = varargin(:);
    param(3:6) = k_def(:);
    
elseif c == 0 % Case 3
    fprintf('Case 3 : Nothing Specified');
    param(1:2) = [{t_def} {C_def}];
    param(3:6) = k_def(:);
    
else % Error due to wrong input
    flag_abort = 1;
    fprintf('Error : No Case Matched');
    output = 'Null';
end

if flag_abort ~= 1 % Output for correct input
    [t C k1 k2 k3 k4] = param{:};
    dCAdt = -k1*C(1)-k2*C(1);
    dCBdt = k1*C(1)-k3-k4*C(2);
    output = [dCAdt dCBdt]; % Output 2 � 1 matrix
end

end % Function ends here
