function varargout = GUI_interface(varargin)
% GUI_INTERFACE MATLAB code for GUI_interface.fig
%      GUI_INTERFACE, by itself, creates a new GUI_INTERFACE or raises the existing
%      singleton*.
%
%      H = GUI_INTERFACE returns the handle to a new GUI_INTERFACE or the handle to
%      the existing singleton*.
%
%      GUI_INTERFACE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI_INTERFACE.M with the given input arguments.
%
%      GUI_INTERFACE('Property','Value',...) creates a new GUI_INTERFACE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI_interface_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI_interface_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI_interface

% Last Modified by GUIDE v2.5 12-Dec-2018 04:06:28

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI_interface_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI_interface_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before GUI_interface is made visible.
function GUI_interface_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI_interface (see VARARGIN)

% Choose default command line output for GUI_interface
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes GUI_interface wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = GUI_interface_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function noode_Callback(hObject, eventdata, handles)
% hObject    handle to noode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function noode_CreateFcn(hObject, eventdata, handles)
% hObject    handle to noode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function noeqn_Callback(hObject, eventdata, handles)
% hObject    handle to noeqn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of noeqn as text
%        str2double(get(hObject,'String')) returns contents of noeqn as a double


% --- Executes during object creation, after setting all properties.
function noeqn_CreateFcn(hObject, eventdata, handles)
% hObject    handle to noeqn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_odes.
function pushbutton_odes_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_odes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.figure1.UserData=GUI_odes;
Nodes=get(handles.noode,'String') %x1 is number of 
Nodes=str2num(Nodes);
save('Nodes.mat','Nodes');
for i=1:Nodes
    run('GUI_odes')
    uiwait
    load('odes.mat')
    odes{i}=strcat('d',numer,'/d',denom,'=',diffeqtn)
handles.diffeqt{i}=diffeqtn
handles.num{i}=numer
handles.den{i}=denom
handles.IC(i)=str2num(ICvalue)
guidata(hObject,handles)
end
set(handles.ODEs,'String',odes)

% end


% --- Executes on button press in pushbutton_eqns.
function pushbutton_eqns_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_eqns (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.figure1.UserData=GUI_eqns;
Nexplicit=get(handles.noeqn,'String')
Nexplicit=str2num(Nexplicit);
save('Neqtns.mat','Nexplicit');

if Nexplicit==0,
    handles.Nexp={}
else
for i=1:Nexplicit
 run('GUI_eqns')
 uiwait
 load('eqtns.mat','expliciteqtn')
 explicit{i}=expliciteqtn;
handles.Nexp{i}=expliciteqtn
guidata(hObject,handles)
end
end
set(handles.Explicit_equations,'String',explicit)
% data_expliciteqtns=expliciteqtn




% --- Executes on button press in pushbutton_solve.
function pushbutton_solve_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_solve (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
tinit=get(handles.tstart,'String');
to=str2num(tinit)
tf=get(handles.tend,'String');
tf=str2num(tf)

[t,y]=ode45(@(t,y)ODE(t,y,handles.diffeqt,handles.num,handles.den,handles.Nexp),[to tf],handles.IC);
t;
y;

handles.t=t;
handles.y=y;
guidata(hObject,handles)

h=msgbox('SYSTEM IS SOLVED')
msg='SYSTEM IS SOLVED'

axes(handles.plots);



function dydt = ODE(t, y,diffeqt,num,den,Nexp)


load('eqtns.mat','expliciteqtn')

% independent variable
 str0=cell2mat(den(1));
 eval(strcat(str0,'= t;'));

 % dependent variables
    for i = 1:length(num)
    str1 = cell2mat(num(i));
    eval(strcat(str1,'= y(i);'));
    end

% explicit equations provided in MATLAB acceptable order, ...
% semicolon separated list

if length(Nexp)>0
 for i = 1:length(Nexp)
 str2 = cell2mat(Nexp(i));
 eval([str2,';']);
 end
end  
 % Right-hand sides of ODE definitions
 for i = 1:length(diffeqt)
    str3 = cell2mat(diffeqt(i));
    dydt(i) = eval(str3);
 end

 dydt = dydt';
 
% --- Executes on button press in pushbutton_show.
function pushbutton_show_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_show (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
load('Nodes.mat','Nodes')
node=Nodes
for i=1:node
p(i)=plot(handles.t,handles.y(:,i),'LineWidth',1.5)
hold all
legend_labels{i} = sprintf('%s', handles.num{i})

end 
hold off
legend(p,legend_labels{:})

xlabel(handles.den(1))
ylabel('y')
% 
% ax=handles.plots
% figure_handle=isolate_axes(ax)
% export_fig plots.png



% fr = getframe(handles.plots);
% imwrite(fr.cdata, 'plots.jpg')
 

% --- Executes on button press in pushbutton_save.
function pushbutton_save_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
 fh=figure;
 copyobj(handles.plots,fh);
 saveas(fh,'plot','png');


function tstart_Callback(hObject, eventdata, handles)
% hObject    handle to tstart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tstart as text
%        str2double(get(hObject,'String')) returns contents of tstart as a double
% global tstart
% tstart=get(hObject,'String');

% --- Executes during object creation, after setting all properties.
function tstart_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tstart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tend_Callback(hObject, eventdata, handles)
% hObject    handle to tend (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tend as text
%        str2double(get(hObject,'String')) returns contents of tend as a double
% global tf
% tf=get(hObject,'String');


% --- Executes during object creation, after setting all properties.
function tend_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tend (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function plots_CreateFcn(hObject, eventdata, handles)
% hObject    handle to plots (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: place code in OpeningFcn to populate plots


% --- Executes on button press in pushbutton_export.
function pushbutton_export_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_export (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB

xlswrite('result.xls',handles.den,'Sheet 1','A1')
xlswrite('result.xls',handles.num,'Sheet 1','B1')
xlswrite('result.xls',handles.t,'Sheet 1','A2')
xlswrite('result.xls',handles.y,'Sheet 1','B2')
% handles    structure with handles and user data (see GUIDATA)

% --- Executes during object creation, after setting all properties.
function ODEs_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ODEs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function Explicit_equations_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Explicit_equations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
