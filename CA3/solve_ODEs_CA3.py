import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint
import math

Ua = 4000; # J/m3.s.C
Ta = 373; # Kelvin
CPA = 90; # J/mol.C
CPB = 90; # J/mol.C
CPC = 180; # J/mol.C
delHR1A = -20000; # J/(mol of A reacted in reaction 1)
delHR2A = -60000; # J/(mol of A reacted in reaction 2)
E1overR = 4000; # Kelvin
E2overR = 9000; # Kelvin
CT0 = 0.1; # mol/dm3
T0 = 423; # Kelvin

########### Function Definition ###########
def ODEs_CA3(F_and_T,V): # Function to define the ODE system
    
    FT = F_and_T[0] + F_and_T[1] + F_and_T[2] # mol/s

    k1A = 10 * np.exp( E1overR * (1/300-1/F_and_T[3]) ) # dm3/mol.s
    k2A = 0.09 * np.exp( E2overR * (1/300-1/F_and_T[3]) ) # dm3/mol.s

    CA = CT0*(F_and_T[0]/FT)*(T0/F_and_T[3]) # mol/dm3
    CB = CT0*(F_and_T[1]/FT)*(T0/F_and_T[3]) # mol/dm3
    CC = CT0*(F_and_T[2]/FT)*(T0/F_and_T[3]) # mol/dm3
    r1A = -k1A*CA 
    r2A = -k2A*CA*CA
    r1B = -r1A
    r2C = -r2A/2
    rA = r1A + r2A
    rB = r1B
    rC = r2C

    dFAdV = rA # ODE 1
    dFBdV = rB # ODE 2
    dFCdV = rC # ODE 3
    dTdV  = ( Ua*(Ta-F_and_T[3]) + (-r1A)*(-delHR1A) + (-r2A)*(-delHR2A) ) / (F_and_T[0]*CPA + F_and_T[1]*CPB + F_and_T[2]*CPC) # ODE 4

    diff = [dFAdV, dFBdV, dFCdV, dTdV]
    return diff # Output of the ODE system function
########### Function ends here ###########

init = [100,0,0,423]; # Initial values 
V = np.linspace(0.0,1.0,101); # Span of depndent variable V in dm3
#print(vspan)
solution = odeint(ODEs_CA3,init,V) # Solving the ODE system


########### Plotting ###########

# Figure 1 plot
plt.figure(figsize=(20,16))
plt.plot(solution[:,3],'-k',linewidth=2.5)
plt.legend([r'$T$'],loc='lower right',fontsize=30)
plt.xlabel(r'$V (dm^3)$', fontsize=30)
plt.ylabel(r'$T(K)$', fontsize=30)
plt.tick_params(axis='both', labelsize=30)
plt.savefig('T.png',dpi=100)
plt.show()

# Figure 2 plot
plt.figure(figsize=(20,16))
plt.plot(V,solution[:,0],'-b',linewidth=2.5)
plt.plot(V,solution[:,1],'--g',linewidth=2.5)
plt.plot(V,solution[:,2],':r',linewidth=2.5)
plt.legend([r'$F_A$',r'$F_B$',r'$F_C$'],loc='upper right',fontsize=30)
plt.xlabel(r'$V (dm^3)$', fontsize=30)
plt.ylabel(r'$F_i$ ($\frac{mol}{s}$)', fontsize=30)
plt.tick_params(axis='both', labelsize=30)
plt.savefig('F.png',dpi=100)
plt.show()
